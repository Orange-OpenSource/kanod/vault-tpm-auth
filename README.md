This service must trade proof of authentication from TPMs with approle secret_id
coresponding to those TPM. The role_id should be provided inside the K8S cluster
by the registrar.

The service executes nearby the Vault with an approle whose policy gives
access to:
* `kv/machines/*` for supporting material about the TPM
* `auth/approle/role/*/secret-id` for the creation of secret ids associated
  to machines.

The configuration is given through environment variables:

* VAULT_ADDR the URL of the Vault api
* VAULT_CACERT the certificate used by the Vault
* SECRET_ID the secret id used by the service to authenticate against the Vault
* ROLE_ID the role id used by the service to authenticate against the Vault
* VAULT_K8S_ROLE can be used instead of SECRET_ID and ROLE_ID to authenticate
  against the Vault
