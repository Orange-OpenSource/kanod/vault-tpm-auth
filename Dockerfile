FROM golang:1.18 as builder

LABEL project=vault-tpm-auth
WORKDIR /workspace
COPY go.mod go.mod
COPY go.sum go.sum
RUN go mod download
COPY main.go main.go
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build -a -o tpm-auth main.go

# Use distroless as minimal base image to package the manager binary
# Refer to https://github.com/GoogleContainerTools/distroless for more details
FROM gcr.io/distroless/static:nonroot
LABEL project="vault-tpm-auth"
WORKDIR /
COPY --from=builder /workspace/tpm-auth .
USER nonroot:nonroot

ENTRYPOINT ["/tpm-auth"]
EXPOSE 3000

