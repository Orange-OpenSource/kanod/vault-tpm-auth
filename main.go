/*
Copyright 2020 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// vault-tpm-auth implements an authentication server that will deliver
// vault secret-id for approle authentication against the proof that the
// requester has the right key which should only be available inside a server
// TPM
//
// vault-tpm-auth does not populate the Vault with the materials used to
// check the authentication handshake. It merely proposes a challenge and
// check the result with what is stored.
//
// vault-tpm-auth requires some access rights on vault to read the
// TPM materials and to create the secret_id.
//
// vault-tpm-auth should not be deployed in the same security context as the
// registrar. Most of the added security relies on the fact that even if one
// of the two brick is compromised, the attacker should still not have a blank
// access tot the Vault because he will only have either the role_id or the
// secret_id but not boths.

package main

import (
	"context"
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"net/http"
	"os"
	"path"

	"github.com/google/uuid"
	vaultapi "github.com/hashicorp/vault/api"
	"github.com/hashicorp/vault/api/auth/approle"
	"github.com/hashicorp/vault/api/auth/kubernetes"
	"go.uber.org/zap"
)

const (
	EnvSecretId       = "SECRET_ID"
	EnvRoleId         = "ROLE_ID"
	EnvPort           = "PORT"
	EnvTlsPath        = "TLS_PATH"
	EnvVaultK8sRole   = "VAULT_K8S_ROLE"
	EnvVaultMountPath = "MOUNT_PATH"
)

type Challenge struct {
	Nonce     string
	PublicKey string
}

type Resp struct {
	Data  string `json:"data,omitempty"`
	Error string `json:"error,omitempty"`
}

type AuthState struct {
	Client        *vaultapi.Client
	Logger        *zap.Logger
	Authentifying map[string]Challenge
}

// getTokenApprole gets a token from the Vault instance.
// It uses an approle with secret_id and role_id supplied by the
// environment variables SECRET_ID and ROLE_ID. The arguments are
// a zap logger for errors and a Vault client structure. The token is stored
// in the client structure
func getTokenApprole(logger *zap.Logger, client *vaultapi.Client) error {
	logger.Info("Getting a token")
	secret_id := os.Getenv(EnvSecretId)
	role_id := os.Getenv(EnvRoleId)
	mountPath := os.Getenv(EnvVaultMountPath)
	if mountPath == "" {
		mountPath = "approle"
	}
	if secret_id == "" || role_id == "" {
		logger.Error("Environment parameters not supplied")
		return fmt.Errorf("needs both secret and role id")
	}
	wrappedSecret := approle.SecretID{FromString: secret_id}
	arAuth, err := approle.NewAppRoleAuth(role_id, &wrappedSecret, approle.WithMountPath(mountPath))
	if err != nil {
		logger.Info("failed to create Vault AppRole auth", zap.Error(err))
		return err
	}
	_, err = client.Auth().Login(context.TODO(), arAuth)
	if err != nil {
		logger.Info("Getting a token failed", zap.Error(err))
	}
	return err
}

// getTokenKubernetes gets a token from the Vault instance and stores it inside
// the provided vault Client. It uses the Kubernetes ServiceAccount authentication.
func getTokenKubernetes(logger *zap.Logger, client *vaultapi.Client, role string) error {
	logger.Info("Getting a token with Kubernetes Auth")

	mountPath := os.Getenv(EnvVaultMountPath)
	// TODO(pc): having a default which is not kubernetes is not a good idea but
	// Supra expects it.
	if mountPath == "" {
		mountPath = "kubernetes/lcm"
	}
	k8sAuth, err := kubernetes.NewKubernetesAuth(role, kubernetes.WithMountPath(mountPath))
	if err != nil {
		return fmt.Errorf("failed to create Vault Kubernetes Auth for role %s: %w", role, err)
	}
	_, err = client.Auth().Login(context.TODO(), k8sAuth)

	if err != nil {
		return fmt.Errorf("failed to login to Vault: %w", err)
	}
	return nil
}

// getToken gets a token from the Vault instance and stores it inside the
// provided vault Client. It supports Kubernetes and Approle authentication,
// depending on the provided environment variables.
func getToken(logger *zap.Logger, client *vaultapi.Client) error {
	if k8sRole := os.Getenv(EnvVaultK8sRole); k8sRole != "" {
		return getTokenKubernetes(logger, client, k8sRole)
	}

	return getTokenApprole(logger, client)
}

// newAuthState creates a state used by vault-auth-tpm.
// It takes a logger as argument */
func newAuthState(logger *zap.Logger) (*AuthState, error) {
	client, err := vaultapi.NewClient(vaultapi.DefaultConfig())
	if err != nil {
		return nil, err
	}
	err = getToken(logger, client)
	if err != nil {
		return nil, err
	}

	return &AuthState{
		Client:        client,
		Logger:        logger,
		Authentifying: make(map[string]Challenge),
	}, nil
}

// readContext returns a TPM context for a given machine.
// The argument is the machine name, ie. the key used to store the context
// and usually the name of the BareMetalHost Kubernetes object on the LCM.
// It returns a map containing the different elements necessary for initializing
// the TPM of the machine with the right key. All those elements are valid only
// with the right TPM
func (s *AuthState) readContext(name string) (map[string]string, error) {
	s.Logger.Info("Reading the context", zap.String("machine", name))
	getToken(s.Logger, s.Client)
	path := "kv/data/machines/" + name
	secret, err := s.Client.Logical().Read(path)
	if err != nil {
		return nil, err
	}
	fmt.Println(secret.Data)
	result := make(map[string]string)
	for k, v := range secret.Data["data"].(map[string]interface{}) {
		result[k] = v.(string)
	}
	return result, nil
}

// secretFromVault gets a secret id for a given machine. There is nothing
// else to do as the capability comes from the policy associated to the
// application.
func (s *AuthState) secretFromVault(name string) (string, error) {
	s.Logger.Info("Creating secret-id", zap.String("machine", name))
	getToken(s.Logger, s.Client)
	path := "auth/approle/role/" + name + "/secret-id"
	resp, err := s.Client.Logical().Write(path, make(map[string]interface{}))
	if err != nil {
		return "", err
	}
	secret, ok := resp.Data["secret_id"]
	if !ok {
		return "", fmt.Errorf("no secret in reply for %s", name)
	}
	return secret.(string), nil
}

// getChallenge handles requests from machine for an authentication challenge
// The machine provides its identity under the parameter name. The program
// answer with a json structure containing a challenge to sign and a TPM
// context for initializing the TPM with the right key.
func (s *AuthState) getChallenge(w http.ResponseWriter, req *http.Request) {
	s.Logger.Info("Get challenge handler")
	params := req.URL.Query()
	bmh_name := params.Get("name")
	if bmh_name == "" {
		http.Error(w, "400 - Missing parameters", http.StatusBadRequest)
		return
	}
	nonce := uuid.New().String()
	context, err := s.readContext(bmh_name)
	if err != nil {
		s.Logger.Warn(
			"Get challenge handler failed",
			zap.String("machine", bmh_name),
			zap.Error(err),
		)
		http.Error(w, "400 - Missing context", http.StatusBadRequest)
		return
	}
	pubKey, ok := context["pub.pem"]
	if !ok {
		http.Error(w, "500 - Missing public key", http.StatusInternalServerError)
		return
	}
	challenge := Challenge{
		Nonce:     nonce,
		PublicKey: pubKey,
	}
	context["nonce"] = nonce
	s.Authentifying[bmh_name] = challenge
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(context)
}

// checkSignature verifies a signed challenge
// The arguments are the name of the machine and the signature. The challenge is
// retrieved from the auth state. An error is returned if the verification
// fails.
func (s *AuthState) checkSignature(bmh_name string, signature string) error {
	s.Logger.Info("Verifying signature", zap.String("machine", bmh_name))
	challenge, ok := s.Authentifying[bmh_name]
	if !ok {
		return fmt.Errorf("unknown machine")
	}
	block, _ := pem.Decode([]byte(challenge.PublicKey))
	if block == nil {
		return fmt.Errorf("no public key found")
	}
	key, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return err
	}
	pubkey := key.(*rsa.PublicKey)
	rawSignature, err := base64.StdEncoding.DecodeString(signature)
	if err != nil {
		return err
	}
	hash := sha256.Sum256([]byte(challenge.Nonce))
	err = rsa.VerifyPKCS1v15(pubkey, crypto.SHA256, hash[:], rawSignature)
	return err
}

// getSecretId returns a secret_id to the machine if the signature provided is correct.
// The name and signature are provided in the query parameter. The answer
// is a json structure containing either an error or the secret_id. */

func (s *AuthState) getSecretId(w http.ResponseWriter, req *http.Request) {
	params := req.URL.Query()
	bmh_name := params.Get("name")
	signature := params.Get("signature")
	if bmh_name == "" || signature == "" {
		http.Error(w, "400 - Missing parameters", http.StatusBadRequest)
		return
	}
	err := s.checkSignature(bmh_name, signature)
	if err == nil {
		s.Logger.Error(
			"Signature verification failed",
			zap.String("machine", bmh_name),
			zap.Error(err),
		)
		http.Error(w, "403 - Forbidden", http.StatusForbidden)
		return
	}
	secret, err := s.secretFromVault(bmh_name)
	if err != nil {
		s.Logger.Error(
			"Could not retrieve a secret-id",
			zap.String("machine", bmh_name),
			zap.Error(err),
		)
		http.Error(w, "500 - Internal Error", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(Resp{Data: secret})
}

func main() {
	logger, _ := zap.NewProduction()
	state, err := newAuthState(logger)
	defer logger.Sync()
	if err != nil {
		fmt.Println("No state ", err)
		return
	}
	http.HandleFunc("/challenge", state.getChallenge)
	http.HandleFunc("/secret_id", state.getSecretId)
	logger.Info("Serving")
	port := os.Getenv(EnvPort)
	if port == "" {
		port = "8800"
	}
	addr := fmt.Sprintf(":%s", port)
	tlsPath := os.Getenv(EnvTlsPath)
	if tlsPath == "" {
		tlsPath = "/mnt/tls"
	}
	certPath := path.Join(tlsPath, "tls.crt")
	keyPath := path.Join(tlsPath, "tls.key")
	_, err1 := os.Stat(certPath)
	_, err2 := os.Stat(keyPath)
	if err1 == nil && err2 == nil {
		err = http.ListenAndServeTLS(addr, certPath, keyPath, nil)
	} else {
		err = http.ListenAndServe(addr, nil)
	}
	logger.Fatal("HTTP server crashed", zap.Error(err))
}
